import sys
import os
from cipher_handler import start_crypto
import time


def load_arguments():
    if len(sys.argv) < 4:
        print("MISSING ARGUMENTS. [1.arg: -c or -dc for cipher or decipher, 2.arg: path to file for key store, "
              "3.arg: path to file to (de)cipher")
        return False

    elif sys.argv[1] != "-c" and sys.argv[1] != "-dc":
        print("1.argument MUST BE FLAG -c OR -dc")
        return False

    elif not os.path.exists(sys.argv[2]) and sys.argv[1] == "-dc":
        print("2.argument IS NOT VALID PATH")
        return False

    elif not sys.argv[2].endswith(".key"):
        print("Path to file with key is MUST ends with .key extension")
        return False

    elif not os.path.exists(sys.argv[3]):
        print("3.argument IS NOT VALID PATH")
        return False

    return True


def create_one_gb_file():
    f = open('files/onegb.txt', "w")
    f.write("tento subor je hrozne velky")
    f.seek(1073741824 - 1)
    f.write("\0")
    f.close()


if __name__ == '__main__':
    start_time = time.time()

    isLoaded = load_arguments()
    if isLoaded:
        start_crypto()

    finish_time = time.time()
    execution_time = finish_time - start_time
    print("PROGRAM FINISHED IN: " + str(execution_time) + "s")




