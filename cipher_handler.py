import sys
from cryptography.fernet import Fernet
import time

def generate_file_key():
    print("Generate key...")

    key = Fernet.generate_key()
    file_name = sys.argv[2]
    with open(file_name, 'wb') as file_key:
        file_key.write(key)

    print("File with key created...")

    return key


def load_key_from_file():
    print("Loading key...")

    file_name = sys.argv[2]
    with open(file_name, 'rb') as file_key:
        key = file_key.read()

    print("Key is loaded from file...")

    return key


def load_target_file(file_name):
    print("Loading target file...")

    with open(file_name, 'rb') as file:
        original = file.read()

    print("Target file loaded...")

    return original


def encrypt_file(key):
    fernet = Fernet(key)
    file_name = sys.argv[3]
    target = load_target_file(file_name)

    print("Start encryption...")
    start_time = time.time()

    encrypted = fernet.encrypt(target)
    with open(file_name, 'wb') as encrypted_file:
        encrypted_file.write(encrypted)

    finish_time = time.time()
    execution_time = finish_time - start_time
    print("Encryption finished in " + str(execution_time))


def decrypt_file(key):
    fernet = Fernet(key)
    file_name = sys.argv[3]
    target = load_target_file(file_name)

    print("Start decryption...")
    start_time = time.time()

    decrypted = fernet.decrypt(target)
    with open(file_name, 'wb') as decrypted_file:
        decrypted_file.write(decrypted)

    finish_time = time.time()
    execution_time = finish_time - start_time
    print("Decryption finished in " + str(execution_time) + " s")


def start_crypto():
    if sys.argv[1] == "-c":
        key = generate_file_key()
        encrypt_file(key)
    elif sys.argv[1] == "-dc":
        key = load_key_from_file()
        decrypt_file(key)

    print("FERNET FINISHED SUCCESSFULLY")
